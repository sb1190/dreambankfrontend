import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

// Ngrx
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

// Modules
import {CoreModule} from '@core/core.module';

// Components
import {AppComponent} from './app.component';

// Routing
import {AppRoutingModule} from './app-routing.module';

// Env
import {environment} from '@env';

// Redux
import {appReducers} from './app.reducers';
import {effectsArr} from './app.effects';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports     : [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // modules
    CoreModule,
    // Ngrx
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({
      maxAge : 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot(effectsArr)
  ],
  providers   : [],
  bootstrap   : [AppComponent]
})
export class AppModule {
}
