import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Modules
import {CapitalLettersModule} from './../../pipes';

// Componente
import {AvatarUserComponent} from './avatar-user.component';

@NgModule({
  declarations: [AvatarUserComponent],
  imports: [
    CommonModule,
    CapitalLettersModule,
    CapitalLettersModule,
    CapitalLettersModule
  ],
  exports     : [AvatarUserComponent],
})
export class AvatarUserModule {
}
