import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector   : 'app-avatar-user',
  templateUrl: './avatar-user.component.html'
})
export class AvatarUserComponent implements OnInit {

  @Input()
  name!: string;
  @Input()
  title!: string;
  @Input()
  size: string;
  @Input()
  fontSize: string;
  @Input()
  fontSizeName: string;
  @Input()
  fontColor!: string;
  @Input()
  viewText: boolean;
  @Input()
  fontSizeSubtitle: string;

  constructor() {
    this.viewText     = false;
    this.size         = '80px';
    this.fontSize     = '16px';
    this.fontSizeName = '12px';
    this.fontColor    = '#000';
  }

  ngOnInit(): void {
  }

}
