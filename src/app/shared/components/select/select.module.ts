import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SelectComponent} from './select.component';
import {ReactiveFormsModule} from '@angular/forms';

// libraries
import {NgSelectModule} from '@ng-select/ng-select';


@NgModule({
  declarations: [SelectComponent],
  imports     : [
    NgSelectModule,
    CommonModule,
    ReactiveFormsModule
  ], exports  : [
    SelectComponent,
    NgSelectModule
  ]
})
export class SelectModule {
}
