import {AvatarUserModule} from './avatar-user/avatar-user.module';
import {SelectComponent} from './select/select.component';


export const SHARED_COMPONENTS = [
  AvatarUserModule,
  SelectComponent
];

export * from './avatar-user';
export * from './select';


