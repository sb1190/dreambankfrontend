export class AccountDetailDomain {
  id: number;
  date: string;
  description: string;
  currency: string;
  value: number;
  balance: number;

}
