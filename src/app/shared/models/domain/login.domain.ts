import {UserDomain} from '@shared/models';

export class LoginDomain {
  accessToken: string;
  refreshToken: string;
  user: UserDomain;
}
