import {ApiResponsePresenter} from '../presenter/api-response.presenter';

export class ErrorDomain {
  status: number | undefined;
  message: string | undefined;
  url: string | undefined;
  error: ApiResponsePresenter | undefined;
}
