export class AccountsDomain {
  id: number;
  type: string;
  name: string;
  status: boolean;
  currency: string;
  balance: number;
}
