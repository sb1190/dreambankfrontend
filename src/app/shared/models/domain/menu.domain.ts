import {OptionsMenuDomain} from './options-menu.domain';

export class MenuDomain {
  id: number;
  title: string;
  options: OptionsMenuDomain[];

}
