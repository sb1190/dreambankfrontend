import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalLetters'
})
export class CapitalLettersPipe implements PipeTransform {

  transform(value: string): any {
    let textValueReturn: string;
    const arrayValue = value ? value.trim().split(' ') : [];
    if (arrayValue.length > 1) {
      textValueReturn = `${arrayValue[0][0]}${arrayValue[1][0]}`;
    } else {
      textValueReturn = value ? value[0][0] : 'N';
    }
    return textValueReturn.toUpperCase();
  }

}
