import { CapitalLettersModule } from './capital-letters/capital-letters.module';

export const SHARED_PIPES = [
    CapitalLettersModule,
];

export { CapitalLettersModule } from './capital-letters/capital-letters.module';
