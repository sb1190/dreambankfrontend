import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

// Ngrx
import {StoreModule} from '@ngrx/store';

// Interceptors
import {INTERCEPTORS} from './interceptors';

// Directive validate forms
import {NgxErrorsModule} from '@app/shared/directives/ngx-errors/ngxerrors.module';

// Pages
import {CORE_PAGES} from '@core/pages';

// Redux
import {Reducers, metaReducers} from './core.reducers';
import {featureKeyAuth} from './redux';

@NgModule({
  declarations: [
    ...CORE_PAGES,
  ],
  imports     : [
    CommonModule,
    ReactiveFormsModule,
    NgxErrorsModule,
    // NGRX
    StoreModule.forFeature(featureKeyAuth, Reducers, {
      metaReducers
    }),
  ],
  exports     : [
    ReactiveFormsModule,
    NgxErrorsModule
  ],
  providers   : [
    ...INTERCEPTORS
  ],
})
export class CoreModule {
}
