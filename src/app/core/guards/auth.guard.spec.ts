import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

// NGRX
import {Store, StoreModule} from '@ngrx/store';

// Components
import {AuthGuard} from './auth.guard';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let store$: Store;

  class StoreMock {
    pipe = jasmine.createSpy()
      .and
      .returnValue(false);
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports  : [
        StoreModule.forRoot({}, {}),
        RouterTestingModule
      ],
      providers: [
        {
          provide : Store,
          useClass: StoreMock
        }
      ]
    });
    store$ = TestBed.inject(Store);
    guard  = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard)
      .toBeTruthy();
  });

});
