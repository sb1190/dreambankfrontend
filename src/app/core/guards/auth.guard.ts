import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

// Rxjs
import {Observable} from 'rxjs';
import {pluck, tap} from 'rxjs/operators';

// NGRX
import {select, Store} from '@ngrx/store';
import {AuthStore} from '@core/redux';

// Redux
import * as fromSelectorAuthStore from '@core/redux';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private readonly store: Store<AuthStore>,
    private readonly router: Router
  ) {
  }

  /**
   * @description The session is validated against the redux store
   * @author <bermu1190@gmail.com>
   * @date 2021-05-17
   */
  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromSelectorAuthStore.selectorUserInformationState),
      pluck('isLogin'),
      tap((isLogin) => {
        if (!isLogin) {
          this.router.navigate(['/login']);
        }
      })
    );
  }

}
