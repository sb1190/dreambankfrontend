import {ActionReducer, ActionReducerMap, Action, MetaReducer} from '@ngrx/store';

import * as fromAuth from '@core/redux';

export const Reducers: ActionReducerMap<fromAuth.AuthStore> = {
  auth: fromAuth.reducer
};

export function clearStore(
  reducer: ActionReducer<fromAuth.AuthStore>
): ActionReducer<any> {
  return (state: fromAuth.AuthStore, action: Action): fromAuth.AuthStore => {
    if (action.type === fromAuth.clearAuthStore.type) {
      state = {
        ...state,
        auth: fromAuth.initialState,
      };
    }
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<fromAuth.AuthStore>[] = [clearStore];
