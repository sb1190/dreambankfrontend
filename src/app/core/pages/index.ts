import {CORE_LOGIN_PAGE} from './login-page';

export const CORE_PAGES = [
  ...CORE_LOGIN_PAGE,
];

export * from './login-page';
