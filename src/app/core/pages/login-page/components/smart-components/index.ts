import {LoginFormComponent} from './login-form/login-form.component';

export const CORE_AUTH_SMART_COMPONENTS = [
  LoginFormComponent,

];

export {LoginFormComponent} from './login-form/login-form.component';
