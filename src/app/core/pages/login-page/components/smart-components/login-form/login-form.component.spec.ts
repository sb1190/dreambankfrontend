import {ComponentFixture, TestBed} from '@angular/core/testing';

// NGRX
import {Store, StoreModule} from '@ngrx/store';

// Rxjs
import {of} from 'rxjs';

// Component
import {LoginFormComponent} from '@core/pages';


describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  let store$: Store;

  class StoreMock {
    select   = jasmine.createSpy()
      .and
      .returnValue(of({}));
    dispatch = jasmine.createSpy();
    pipe     = jasmine.createSpy()
      .and
      .returnValue(of('success'));
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginFormComponent],
      imports     : [
        StoreModule.forRoot({}, {}),
      ],
      providers   : [
        {
          provide : Store,
          useClass: StoreMock
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    store$    = TestBed.inject(Store);
    fixture   = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Login Component', () => {
    expect(component)
      .toBeTruthy();
  });

  it('should not must issue a dispatch', () => {
    component.login();
    expect(store$.dispatch)
      .not
      .toHaveBeenCalled();
  });

  it('should must issue a dispatch', () => {
    component.form.controls.dni.setValue('TestDni');
    component.form.controls.password.setValue('TestPassword');
    component.login();
    expect(store$.dispatch)
      .toHaveBeenCalled();
  });

  it('should return the value of the control', () => {
    const valuePassword = 'TestPassword';
    component.form.controls.password.setValue(valuePassword);
    expect(component.password.value)
      .toEqual(valuePassword);
  });

});
