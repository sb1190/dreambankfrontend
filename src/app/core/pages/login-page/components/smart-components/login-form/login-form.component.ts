import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';

// Rxjs
import {Subscription} from 'rxjs';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AuthStore, UserInformationState} from '@core/redux';

// Models
import {ErrorDomain} from '@app/shared/models';

// Redux
import * as authAction from '@core/redux/auth.actions';
import * as fromSelectorAuthStore from '@core/redux';

@Component({
  selector   : 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls  : ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {

  form: FormGroup;
  loading: boolean;
  error: ErrorDomain;
  subscribes: Subscription[];

  constructor(
    private readonly store: Store<AuthStore>
  ) {
    this.form       = new FormGroup({});
    this.loading    = false;
    this.error      = new ErrorDomain();
    this.subscribes = [];
  }

  ngOnInit(): void {
    this.formLogin();
    this.subscribeStore();
  }

  ngOnDestroy(): void {
    this.subscribes.forEach((sub) => sub.unsubscribe());
  }

  public login(): void {
    if (this.form.valid) {
      this.store.dispatch(
        authAction.setUser({...this.form.value})
      );
    }
  }

  private formLogin(): void {
    this.form = new FormGroup({
      dni     : new FormControl('', [Validators.required]),
      password: new FormControl('', Validators.required),
    });
  }

  get dni(): AbstractControl {
    return this.form.get('din');
  }

  get password(): AbstractControl {
    return this.form.get('password');
  }

  private subscribeStore(): void {
    this.subscribes.push(
      this.store.pipe(select(fromSelectorAuthStore.selectorUserInformationState))
        .subscribe((auth: UserInformationState) => this.userAuthresponse(auth))
    );
  }

  private userAuthresponse(auth: UserInformationState): void {
    this.loading = auth.loading;
    auth.isLogin ? this.form.reset() : this.error = auth.error;
  }


}
