import {LoginPageComponent} from './login-page.component';

import {CORE_AUTH_SMART_COMPONENTS} from './components/smart-components';

export const CORE_LOGIN_PAGE = [
  ...CORE_AUTH_SMART_COMPONENTS,
  LoginPageComponent
];

export {LoginPageComponent} from './login-page.component';
export * from './components/smart-components';
