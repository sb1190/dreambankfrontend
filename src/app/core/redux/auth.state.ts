// Models
import {LoginDomain} from '@shared/models';

// State principal
import {AppState} from '@app/app.reducers';

export interface UserInformationState {
  login: LoginDomain;
  isLogin: boolean;
  loaded: boolean;
  loading: boolean;
  error: any;
}

export interface AuthStore extends AppState {
  auth: UserInformationState;
}

export const initialState: UserInformationState = {
  login  : null,
  isLogin: false,
  loaded : false,
  loading: false,
  error  : null,
};
