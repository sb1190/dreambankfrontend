import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Router} from '@angular/router';

import * as AuthActions from './auth.actions';
import {of} from 'rxjs';
import {map, catchError, mergeMap} from 'rxjs/operators/';
import {AuthService} from '@http-services';

// Models
import {ApiResponsePresenter, LoginDomain} from '@shared/models';

@Injectable()
export class AuthEffects {

  constructor(
    private readonly actions$: Actions,
    public readonly authService: AuthService,
    private readonly router: Router
  ) {
  }

  setUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.setUser),
      mergeMap((action) =>
        this.authService.login(action.dni, action.password)
          .pipe(
            map((response: ApiResponsePresenter) => {
              const login: LoginDomain = response.data;
              this.router.navigate(['/home/accounts']);
              return AuthActions.setUserSuccess({login});
            }),
            catchError((error) => of(AuthActions.setUserFail(error))
            )
          )
      )
    )
  );

  logoutUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logoutUser),
      map(() => AuthActions.logoutUserSuccess())
    )
  );


}
