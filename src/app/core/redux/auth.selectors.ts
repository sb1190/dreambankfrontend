import {createFeatureSelector, createSelector} from '@ngrx/store';

import {AuthStore} from '@core/redux/auth.state';
import {featureKeyAuth} from './auth.key.reducer';

export const selectorAuthState = createFeatureSelector<AuthStore>(
  featureKeyAuth
);

export const selectorUserInformationState = createSelector(
  selectorAuthState,
  state => state.auth
);

export const selectorUser = createSelector(
  selectorUserInformationState,
  state => state.login
);
