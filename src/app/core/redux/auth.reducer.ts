import * as fromAuth from './auth.actions';
import {on, createReducer, Action, ActionReducer} from '@ngrx/store';
import {initialState, UserInformationState} from '@core/redux/auth.state';

const UserInformationReducer = createReducer(
  initialState,
  on(fromAuth.setUser, (state) => ({...state, loading: true, error: null})),
  on(fromAuth.setUserSuccess, (state, {login}) => ({
    ...state,
    loading: false,
    isLogin: true,
    loaded : true,
    login   : {...login},
  })),
  on(fromAuth.setUserFail, (state, {status, message, url, error}) => ({
    ...state,
    loaded : false,
    isLogin: false,
    loading: false,
    error  : {
      status,
      message,
      url,
      error,
    },
  })),
  on(fromAuth.logoutUser, (state) => ({
    ...state,
    loading: true,
  })),
  on(fromAuth.logoutUserSuccess, (state) => ({
    ...state,
    loading: false,
    isLogin: false,
    loaded : true,
    login   : null,
  })),
  on(fromAuth.setError, (state) => ({
    ...state,
    error: null,
  }))
);

export const reducer: ActionReducer<any, any> = (state: UserInformationState, action: Action) => UserInformationReducer(state, action);

