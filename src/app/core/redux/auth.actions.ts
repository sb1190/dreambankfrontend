import {createAction, props} from '@ngrx/store';

// Models
import {LoginDomain, ErrorDomain} from '@shared/models';

export const setUser = createAction(
  '[UserInformation] Set User',
  props<{
    dni: string;
    password: string;
  }>()
);

export const setUserSuccess = createAction(
  '[UserInformation] Set user SUCCESS',
  props<{
    login: LoginDomain;
  }>()
);

export const setUserFail = createAction(
  '[UserInformation] Set user FAIL',
  props<{
    status: number;
    url: string;
    message: string;
    error: ErrorDomain;
  }>()
);

export const logoutUser = createAction('[Auth] Logout user');

export const logoutUserSuccess = createAction('[Auth] Logout user SUCCESS');

export const setError = createAction('[UserInformation] set ERROR');

export const clearAuthStore = createAction(
  '[ UserInformation ] Set Clear Store'
);
