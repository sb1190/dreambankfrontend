import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';

// Rxjs
import {Observable, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';

// Environments
import {environment} from '@env';

@Injectable()
export class ValidateRequestService implements HttpInterceptor {

  constructor() {
  }

  /**
   * @description Retry the request if there is an error code
   * @author <bermu1190@gmail.com>
   * @date 2021-05-17
   */
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        retry(1),
        catchError((error) => this.handleError(error))
      );
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (!environment.production) {
      console.log(error);
    }
    return throwError(error);
  }

}





