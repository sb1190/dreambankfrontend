import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {ValidateRequestService} from './validate-request.service';

describe('ValidateRequestService', () => {
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ValidateRequestService,
          multi: true,
        },
      ]
    });
    httpMock = TestBed.inject(HttpTestingController);
  });


});
