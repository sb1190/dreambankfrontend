import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {TokenService} from './token.service';
import {Store, StoreModule} from '@ngrx/store';
import {of} from 'rxjs';
import {AccountService, AuthService} from '@http-services';
import {RouterTestingModule} from '@angular/router/testing';


describe('Token Service', () => {

  let httpTestingController: HttpTestingController;
  let store$: Store;
  let authService: AuthService;

  class StoreMock {
    select   = jasmine.createSpy()
      .and
      .returnValue(of({}));
    dispatch = jasmine.createSpy();
    pipe     = jasmine.createSpy()
      .and
      .returnValue(of('success'));
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports  : [
        HttpClientTestingModule,
        RouterTestingModule,
        StoreModule.forRoot({}, {})
      ],
      providers: [
        AccountService,
        AuthService,
        {
          provide : HTTP_INTERCEPTORS,
          useClass: TokenService,
          multi   : true,
        },
        {
          provide : Store,
          useClass: StoreMock
        }
      ]
    });
    store$                = TestBed.inject(Store);
    httpTestingController = TestBed.inject(HttpTestingController);
    authService           = TestBed.inject(AuthService);
  });

  it('should not add an Authorization header', () => {
    authService.login('TestDni', 'TestPassword')
      .subscribe(res => expect(res)
        .toBeTruthy()
      );
    const httpRequest = httpTestingController.expectOne(`${authService.url}/auth/login`);
    expect(httpRequest.request.headers.get('Authorization'))
      .toEqual(null);
  });


});
