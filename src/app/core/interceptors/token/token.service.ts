import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';

// rxjs
import {Observable, Subscription, throwError} from 'rxjs';
import {catchError, concatMap} from 'rxjs/operators';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AuthStore} from '@core/redux';

// Redux
import * as fromSelectorAuthStore from '@core/redux';

// Env
import {environment} from '@env';

@Injectable()
export class TokenService implements HttpInterceptor, OnDestroy {

  private url = `${environment.apiUrl}/auth/refresh`;
  private subscribes: Subscription[];
  private accessToken: string;
  private refreshToken: string;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly store: Store<AuthStore>
  ) {
    this.subscribes   = [];
    this.accessToken  = '';
    this.refreshToken = '';
    this.subscribeStore();
  }

  ngOnDestroy(): void {
    this.subscribes.forEach((sub) => sub.unsubscribe());
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    if (this.accessToken) {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Authorization : this.accessToken
        }
      });
    } else {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
        }
      });
    }
    return next.handle(req)
      .pipe(
        catchError(error => {
          const tokenExpired = error.status === 401 && error.error.msg === 'Token has expired';
          return tokenExpired
            ? this.addToken(req, next)
            : throwError(error);
        })
      );
  }

  /**
   * @description Refresh the token if it is expired
   * @author <bermu1190@gmail.com>
   * @date 2021-05-17
   */
  private addToken(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this.accessToken = '';
    return this.httpClient.post(this.url, null, {headers: this.addHttpOptions()})
      .pipe(
        concatMap((resp: any) => {
          if (resp.success) {
            req = req.clone({
              setHeaders: {
                'Content-Type': 'application/json',
                Authorization : this.accessToken
              }
            });
          }
          return next.handle(req);
        })
      );
  }

  private addHttpOptions(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization : this.refreshToken
    });
  }

  private subscribeStore(): void {
    this.subscribes.push(
      this.store.pipe(select(fromSelectorAuthStore.selectorUserInformationState))
        .subscribe(({login}) => {
          this.accessToken  = login?.accessToken;
          this.refreshToken = login?.refreshToken;
        })
    );
  }

}
