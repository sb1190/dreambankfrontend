import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {TokenService} from './token/token.service';
import {ValidateRequestService} from './validate-request/validate-request.service';

export const INTERCEPTORS = [
  {
    provide : HTTP_INTERCEPTORS,
    useClass: TokenService,
    multi   : true
  },
  {
    provide : HTTP_INTERCEPTORS,
    useClass: ValidateRequestService,
    multi   : true
  }
];

export {TokenService} from './token/token.service';
export {ValidateRequestService} from './validate-request/validate-request.service';
