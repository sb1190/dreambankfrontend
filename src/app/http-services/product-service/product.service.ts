import {Injectable} from '@angular/core';

// Env
import {environment} from '@env';
import {Observable} from 'rxjs';
import {ApiResponsePresenter, RequestProductDomain} from '@shared/models';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url: string;

  constructor(private readonly http: HttpClient) {
    this.url = `${environment.apiUrl}`;
  }

  public requestNewProduct(product: RequestProductDomain): Observable<ApiResponsePresenter> {
    return this.http.post<ApiResponsePresenter>(
      `${environment.apiUrl}/request-product`,
      JSON.stringify(product)
    );
  }

}
