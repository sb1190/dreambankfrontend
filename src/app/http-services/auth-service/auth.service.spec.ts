import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

// Rxjs
import {pluck} from 'rxjs/operators';

// Components
import {AuthService} from './auth.service';
import {ApiResponsePresenter, LoginDomain} from '@shared/models';

describe('AuthService', () => {
  let httpTestingController: HttpTestingController;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports  : [HttpClientTestingModule, RouterTestingModule],
      providers: [AuthService]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    authService           = TestBed.inject(AuthService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created AuthService', () => {
    expect(authService)
      .toBeTruthy();
  });

  it('should be return the login user', () => {
    const mockAuthService: ApiResponsePresenter = {
      success: true,
      message: '',
      data   : {
        refreshToken: 'TestRefreshToken',
        accessToken : 'TestAccessToken',
        user        : {
          id      : 1,
          fullName: '',
          dni     : ''
        }
      }
    };
    authService.login('dniTest', 'passwordTest')
      .pipe(pluck('data'))
      .subscribe((login: LoginDomain) => {
        console.log(login);
        expect(login.refreshToken)
          .toEqual('TestRefreshToken');
        expect(login.accessToken)
          .toEqual('TestAccessToken');
        expect(login.user.id)
          .toEqual(1);
      });
    httpTestingController.expectOne(`${authService.url}/auth/login`)
      .flush(mockAuthService);

  });
});
