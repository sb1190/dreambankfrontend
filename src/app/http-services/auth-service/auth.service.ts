import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


// Rxjs
import {Observable} from 'rxjs';

// Models
import {ApiResponsePresenter} from '@shared/models';

// Env
import {environment} from '@env';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string;

  constructor(
    private readonly http: HttpClient
  ) {
    this.url = `${environment.apiUrl}`;
  }

  public login(
    dni: string,
    password: string
  ): Observable<ApiResponsePresenter> {
    return this.http.post<ApiResponsePresenter>(
      `${environment.apiUrl}/auth/login`,
      JSON.stringify({dni, password})
    );
  }


}
