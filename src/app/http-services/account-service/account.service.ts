import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// Rxjs
import {Observable} from 'rxjs';
import {pluck} from 'rxjs/operators';

// Env
import {environment} from '@env';

// models
import {AccountDetailDomain, AccountsDomain} from '@shared/models';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  url: string;

  constructor(
    private readonly http: HttpClient
  ) {
    this.url = `${environment.apiUrl}`;
  }

  public getUserAccounts(id: number): Observable<AccountsDomain[]> {
    return this.http.get<AccountsDomain>(`${environment.apiUrl}/account/${id}`)
      .pipe(
        pluck('data')
      );
  }

  public getAccountDetail(id: number): Observable<AccountDetailDomain[]> {
    return this.http.get<AccountDetailDomain>(`${environment.apiUrl}/account-detail/${id}`)
      .pipe(
        pluck('data')
      );
  }

}
