import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

// Components
import {AccountService} from './account.service';
import {AccountDetailDomain, AccountsDomain, ApiResponsePresenter, LoginDomain} from '@shared/models';
import {pluck} from 'rxjs/operators';

describe('AccountService', () => {
  let accountService: AccountService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports  : [HttpClientTestingModule],
      providers: [AccountService]
    });
    accountService        = TestBed.inject(AccountService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(accountService)
      .toBeTruthy();
  });

  it('should be get accounts', () => {
    const mockAccountService: ApiResponsePresenter = {
      success: true,
      message: '',
      data   : {
        accounts: [
          {
            id      : 1,
            type    : 'TestType',
            name    : 'TestName',
            status  : true,
            currency: 'TestCurrency',
            balance : 100000,
          }
        ]
      }
    };

    accountService.getUserAccounts(1)
      .pipe(pluck('accounts'))
      .subscribe((accounts: AccountsDomain[]) => {
        expect(accounts[0].id)
          .toEqual(1);
        expect(accounts[0].type)
          .toEqual('TestType');
        expect(accounts[0].name)
          .toEqual('TestName');
      });
    httpTestingController.expectOne(`${accountService.url}/account/${1}`)
      .flush(mockAccountService);
  });

  it('should be get accounts detail', () => {
    const mockAccountService: ApiResponsePresenter = {
      success: true,
      message: '',
      data   : {
        accountDetail: [
          {
            id         : 1,
            date       : 'TestDate',
            description: 'TestDescription',
            currency   : 'TestCurrency',
            value      : 100000,
            balance    : 100000,
          }
        ]
      }
    };

    accountService.getAccountDetail(1)
      .pipe(pluck('accountDetail'))
      .subscribe((accountsDetail: AccountDetailDomain[]) => {
        expect(accountsDetail[0].id)
          .toEqual(1);
        expect(accountsDetail[0].date)
          .toEqual('TestDate');
        expect(accountsDetail[0].description)
          .toEqual('TestDescription');
      });
    httpTestingController.expectOne(`${accountService.url}/account-detail/${1}`)
      .flush(mockAccountService);
  });


});
