import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Components
import {AccountsPageComponent, TransactionDetailPageComponent} from './features/accounts/pages';
import {DashboardComponent} from '@features/dashboard/dashboard.component';

const routes: Routes = [

  {
    path     : 'home',
    component: DashboardComponent,
    children : [
      {
        path     : 'accounts',
        component: AccountsPageComponent,
        data     : {title: 'accounts'}
      },
      {
        path     : 'transaction-detail/:id',
        component: TransactionDetailPageComponent,
        data     : {title: 'accounts'}
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
