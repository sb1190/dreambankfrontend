import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

// Modules
import {AvatarUserModule} from '@shared/components';

// Components
import {DASHBOARD_COMPONENTS} from './components';
import {DashboardComponent} from './dashboard.component';

// Dashboard
import {DashboardRoutingModule} from './dashboard-routing.module';
import {AccountsModule} from '@features/dashboard/features/accounts/accounts.module';


@NgModule({
  declarations: [
    DashboardComponent,
    ...DASHBOARD_COMPONENTS,
  ],
  imports     : [
    CommonModule,
    RouterModule,
    // Modules
    AvatarUserModule,
    AccountsModule,
    // Router
    DashboardRoutingModule
  ], exports  : []
})
export class DashboardModule {
}
