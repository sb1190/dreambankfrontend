import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

// Rxjs
import {Observable, throwError} from 'rxjs';
import {catchError, concatMap, pluck} from 'rxjs/operators';

// Models
import {AccountsDomain} from '@shared/models';

// Services
import {AccountService} from '@http-services';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AuthStore} from '@core/redux';
import * as fromSelectorAuthStore from '@core/redux';


@Component({
  selector   : 'app-table-accounts',
  templateUrl: './table-accounts.component.html',
  styleUrls  : ['./table-accounts.component.scss']
})
export class TableAccountsComponent implements OnInit {

  userAccountList$: Observable<AccountsDomain[] | any>;
  public errorObject = null;

  constructor(
    private readonly accountService: AccountService,
    private readonly store: Store<AuthStore>,
    private readonly router: Router
  ) {

  }

  ngOnInit(): void {
    this.getUserAccountList();
  }

  public getUserAccountList(): void {
    this.userAccountList$ = this.store.pipe(
      select(fromSelectorAuthStore.selectorUser),
      concatMap(({accessToken, refreshToken, user}) => this.accountService.getUserAccounts(user.id)),
      pluck('accounts'),
      catchError(err => {
        this.errorObject = err;
        return throwError(err);
      })
    );
  }

  public viewDetail(account: AccountsDomain): void {
    this.router.navigate([`/home/transaction-detail/${account.id}`], {
      state: {account}
    });
  }


}
