import {TableAccountsComponent} from './table-accounts/table-accounts.component';

export const ACCOUNTS_SMART_COMPONENTS = [
  TableAccountsComponent,
];

export {TableAccountsComponent} from './table-accounts/table-accounts.component';
