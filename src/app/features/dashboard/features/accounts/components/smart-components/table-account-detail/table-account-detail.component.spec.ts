import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TableAccountDetailComponent} from './table-account-detail.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Store, StoreModule} from '@ngrx/store';
import {of} from 'rxjs';
import {AccountService} from '@app/http-services';

describe('TableAccountDetailComponent', () => {
  let component: TableAccountDetailComponent;
  let fixture: ComponentFixture<TableAccountDetailComponent>;
  let accountService: AccountService;
  let store$: Store;

  class StoreMock {
    select   = jasmine.createSpy()
      .and
      .returnValue(of({}));
    dispatch = jasmine.createSpy();
    pipe     = jasmine.createSpy()
      .and
      .returnValue(of([]));
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableAccountDetailComponent],
      imports     : [
        HttpClientTestingModule,
        StoreModule.forRoot({}, {}),
      ],
      providers   : [
        {
          provide : Store,
          useClass: StoreMock
        },
        AccountService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    store$         = TestBed.inject(Store);
    fixture        = TestBed.createComponent(TableAccountDetailComponent);
    accountService = TestBed.inject(AccountService);
    component      = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component)
      .toBeTruthy();
  });

});
