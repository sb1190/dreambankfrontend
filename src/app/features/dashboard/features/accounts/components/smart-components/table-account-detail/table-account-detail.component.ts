import {Component, OnInit} from '@angular/core';

// Rxjs
import {Observable, throwError} from 'rxjs';
import {catchError, concatMap, pluck} from 'rxjs/operators';

// Models
import {AccountDetailDomain} from '@shared/models';

// Services
import {AccountService} from '@http-services';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AuthStore} from '@core/redux';
import * as fromSelectorAuthStore from '@core/redux';


@Component({
  selector   : 'app-table-account-detail',
  templateUrl: './table-account-detail.component.html',
  styleUrls  : ['./table-account-detail.component.scss']
})
export class TableAccountDetailComponent implements OnInit {

  DetailAccountList$: Observable<AccountDetailDomain[] | any>;
  public errorObject = null;

  constructor(
    private readonly accountService: AccountService,
    private readonly store: Store<AuthStore>
  ) {

  }

  ngOnInit(): void {
    this.getUserAccountList();
  }

  private getUserAccountList(): void {
    this.DetailAccountList$ = this.store.pipe(
      select(fromSelectorAuthStore.selectorUser),
      concatMap(({accessToken, refreshToken, user}) => this.accountService.getAccountDetail(user.id)),
      pluck('accountDetail'),
      catchError(err => {
        this.errorObject = err;
        return throwError(err);
      })
    );
  }
}
