import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TableAccountsComponent} from './table-accounts.component';
import {Store, StoreModule} from '@ngrx/store';
import {of} from 'rxjs';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';


describe('TableAccountsComponent', () => {
  let component: TableAccountsComponent;
  let fixture: ComponentFixture<TableAccountsComponent>;
  let store$: Store;

  class StoreMock {
    select   = jasmine.createSpy()
      .and
      .returnValue(of({}));
    dispatch = jasmine.createSpy();
    pipe     = jasmine.createSpy()
      .and
      .returnValue(of([]));
  }


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableAccountsComponent],
      imports     : [
        HttpClientTestingModule,
        RouterTestingModule,
        StoreModule.forRoot({}, {}),
      ],
      providers   : [
        {
          provide : Store,
          useClass: StoreMock
        },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    store$    = TestBed.inject(Store);
    fixture   = TestBed.createComponent(TableAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component)
      .toBeTruthy();
  });
});
