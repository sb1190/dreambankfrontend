import {TransactionDetailPageComponent} from '@features/dashboard/features/accounts/pages/transaction-detail-page/transaction-detail-page.component';
import {AccountsPageComponent} from './accounts-page/accounts-page.component';


export const ACCOUNTS_PAGES = [
  AccountsPageComponent,
  TransactionDetailPageComponent
];

export {TransactionDetailPageComponent} from '@features/dashboard/features/accounts/pages/transaction-detail-page/transaction-detail-page.component';
export {AccountsPageComponent} from './accounts-page/accounts-page.component';
