import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

// Models
import {AccountsDomain} from '@shared/models';


@Component({
  selector   : 'app-transaction-detail-page',
  templateUrl: './transaction-detail-page.component.html',
  styleUrls  : ['./transaction-detail-page.component.scss']
})
export class TransactionDetailPageComponent implements OnInit {

  accountDetail: AccountsDomain;

  constructor(
    private readonly location: Location,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {
    this.accountDetail = this.router.getCurrentNavigation().extras.state.account;
  }

  ngOnInit(): void {
  }

  public back(): void {
    this.location.back();
  }

}
