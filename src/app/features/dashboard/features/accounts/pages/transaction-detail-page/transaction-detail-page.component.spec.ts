import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TransactionDetailPageComponent} from './transaction-detail-page.component';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';

class RouterStub {
  getCurrentNavigation = () => ({
    extras: {
      state: {
        account: {},
      }
    }
  });
}

describe('TransactionDetailPageComponent', () => {
  let component: TransactionDetailPageComponent;
  let fixture: ComponentFixture<TransactionDetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TransactionDetailPageComponent],
      imports     : [RouterTestingModule],
      providers   : [
        {
          provide : Router,
          useClass: RouterStub

        }
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture   = TestBed.createComponent(TransactionDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  xit('should create', () => {
    expect(component)
      .toBeTruthy();
  });
});
