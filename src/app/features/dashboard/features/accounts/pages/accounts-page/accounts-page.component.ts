import {Component, OnInit} from '@angular/core';


// Libraries
import * as d3 from 'd3';

@Component({
  selector   : 'app-accounts-page',
  templateUrl: './accounts-page.component.html',
  styleUrls  : ['./accounts-page.component.scss']
})
export class AccountsPageComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
