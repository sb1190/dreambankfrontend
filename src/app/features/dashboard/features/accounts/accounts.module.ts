import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Components
import {ACCOUNTS_PAGES} from './pages';
import {ACCOUNTS_COMPONENTS} from './components';
import { TableAccountsComponent } from './components/smart-components/table-accounts/table-accounts.component';
import { TableAccountDetailComponent } from './components/smart-components/table-account-detail/table-account-detail.component';


@NgModule({
  declarations: [
    ...ACCOUNTS_COMPONENTS,
    ...ACCOUNTS_PAGES,
    TableAccountsComponent,
    TableAccountDetailComponent,
  ],
  imports     : [
    CommonModule
  ],
  exports     : [
    ...ACCOUNTS_COMPONENTS,
    ...ACCOUNTS_PAGES,
  ]
})
export class AccountsModule {
}
