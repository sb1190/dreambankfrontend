import {ChangeDetectionStrategy, Component, ContentChild, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector       : 'app-template',
  templateUrl    : './template.component.html',
  styleUrls      : ['./template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateComponent implements OnInit {

  @ContentChild('contentTemplate') contentTemplate: TemplateRef<any>;

  constructor() {
  }

  ngOnInit(): void {
  }

}
