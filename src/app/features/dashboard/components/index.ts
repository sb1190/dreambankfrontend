
import {DASHBOARD_DUMB_COMPONENTS} from './dumb-components';
import { DASHBOARD_SMART_COMPONENTS } from './smart-components';

export const DASHBOARD_COMPONENTS = [
  ...DASHBOARD_SMART_COMPONENTS,
  ...DASHBOARD_DUMB_COMPONENTS
];

export * from './smart-components';
export * from './dumb-components';


