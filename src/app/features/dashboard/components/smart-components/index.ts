import {UserBarComponent} from './user-bar/user-bar.component';
import {NavbarComponent} from './navbar/navbar.component';
import {SidebarComponent} from './sidebar/sidebar.component';

export const DASHBOARD_SMART_COMPONENTS = [
  UserBarComponent,
  NavbarComponent,
  SidebarComponent
];

export {UserBarComponent} from './user-bar/user-bar.component';
export {NavbarComponent} from './navbar/navbar.component';
export {SidebarComponent} from './sidebar/sidebar.component';
