import {Component} from '@angular/core';

// Models
import {MenuDomain} from '@shared/models';

@Component({
  selector   : 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls  : ['./sidebar.component.scss']
})
export class SidebarComponent {

  menu: MenuDomain[];

  constructor() {
    this.menu = this.menuList();
  }

  private menuList(): MenuDomain[] {
    return [
      {
        id     : 1,
        title  : 'ACCOUNTS',
        options: [
          {id: 1, name: 'Account Summary', path: '/home/accounts'},
          {id: 2, name: 'Accounts', path: '/home/accounts/accounts'}
        ]
      },
      {
        id     : 2,
        title  : 'TRANSACTIONS',
        options: [
          {id: 1, name: 'Inquire Transactions', path: '/home/transactions'},
          {id: 2, name: 'Fund Transfer', path: '/home/transactions'},
          {id: 3, name: 'Bill Payments', path: '/home/transactions'}
        ]
      },
      {
        id     : 3,
        title  : 'SERVICES',
        options: [
          {id: 1, name: 'Account Statements', path: '/home/services'},
          {id: 2, name: 'Enroll New Account', path: '/home/services'},
          {id: 3, name: 'Enroll a Credit Card', path: '/home/services'},
          {id: 4, name: 'Card Replacement', path: '/home/services'},
          {id: 5, name: 'New Checkbook', path: '/home/services'}
        ]
      }
    ];
  }

}
