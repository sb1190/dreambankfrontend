import {NewProductpageComponent} from '@features/products/pages/new-productpage/new-productpage.component';
import {SuccessProductPageComponent} from '@features/products/pages/success-product-page/success-product-page.component';


export const PRODUCTS_PAGES = [
  NewProductpageComponent,
  SuccessProductPageComponent
];


export {NewProductpageComponent} from '@features/products/pages/new-productpage/new-productpage.component';
export {SuccessProductPageComponent} from '@features/products/pages/success-product-page/success-product-page.component';
