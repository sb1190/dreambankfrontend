import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessProductPageComponent } from './success-product-page.component';

describe('SuccessProductPageComponent', () => {
  let component: SuccessProductPageComponent;
  let fixture: ComponentFixture<SuccessProductPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuccessProductPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessProductPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
