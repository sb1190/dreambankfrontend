import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

// Rxjs
import {Subscription} from 'rxjs';
import {pluck} from 'rxjs/operators';

// Services
import {ProductService} from '@app/http-services';

// Models
import {ErrorDomain, RequestProductDomain} from '@shared/models';

@Component({
  selector   : 'app-new-product-form',
  templateUrl: './new-product-form.component.html',
  styleUrls  : ['./new-product-form.component.scss'],
})
export class NewProductFormComponent implements OnInit {

  cities = [
    {id: 1, name: 'Foreign Currency Exchange.'},
    {id: 2, name: 'Bank Guarantee.'},
    {id: 3, name: 'Remittance of Funds.'},
    {id: 4, name: 'Credit cards.'},
    {id: 5, name: 'Debit cards.'},
  ];

  form: FormGroup;
  loading: boolean;
  error: ErrorDomain;
  subscribes: Subscription[];

  constructor(
    private readonly productService: ProductService,
    private readonly router: Router
  ) {
    this.form       = new FormGroup({});
    this.loading    = false;
    this.error      = new ErrorDomain();
    this.subscribes = [];
  }

  ngOnInit(): void {
    this.formNewProduct();
  }

  public createNewProduct(): void {
    if (this.form.valid) {
      this.productService.requestNewProduct({...this.form.value})
        .pipe(pluck('data'))
        .subscribe({
          next : (requestProduct: RequestProductDomain) => this.requestProductSuccess(requestProduct),
          error: (err => this.error = err)
        });
    }
  }

  private requestProductSuccess(requestProduct: RequestProductDomain): void {
    if (requestProduct.id) {
      this.router.navigate(['/success-product']);
    }
  }

  private formNewProduct(): void {
    this.form = new FormGroup({
      id       : new FormControl('', [Validators.required]),
      cellphone: new FormControl('', Validators.required),
      monthly  : new FormControl('', Validators.required),
    });
  }

}
