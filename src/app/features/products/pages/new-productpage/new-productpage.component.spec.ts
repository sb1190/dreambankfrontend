import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductpageComponent } from './new-productpage.component';

describe('NewProductpageComponent', () => {
  let component: NewProductpageComponent;
  let fixture: ComponentFixture<NewProductpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewProductpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
