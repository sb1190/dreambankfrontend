import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';


// pages
import {PRODUCTS_PAGES} from './pages';

// Router
import {ProductsRoutingModule} from '@features/products/products-routing.module';
import {NewProductFormComponent} from './pages/new-productpage/components/smart-components/new-product-form/new-product-form.component';

// Modules
import {SelectModule} from '@app/shared/components';
import {NgxErrorsModule} from '@shared/directives/ngx-errors/ngxerrors.module';


@NgModule({
  declarations: [
    ...PRODUCTS_PAGES,
    NewProductFormComponent
  ],
  imports     : [
    CommonModule,
    // Modules
    SelectModule,
    ReactiveFormsModule,
    NgxErrorsModule,
    // Router
    ProductsRoutingModule,
  ]
})
export class ProductsModule {
}
