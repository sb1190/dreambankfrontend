import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


import {
  NewProductpageComponent,
  SuccessProductPageComponent
} from '@features/products/pages';


const routes: Routes = [
  {
    path     : 'new-product',
    component: NewProductpageComponent,
    data     : {title: 'new-product'}
  },
  {
    path     : 'success-product',
    component: SuccessProductPageComponent,
    data     : {title: 'success-product'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule {
}
