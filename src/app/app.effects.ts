import { AuthEffects } from '@core/redux/auth.effects';

export const effectsArr: any [] = [AuthEffects];

export * from '@core/redux/auth.effects';
