import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Components
import {LoginPageComponent} from '@core/pages/login-page';
import {AuthGuard} from '@core/guards';


const routes: Routes = [
  {
    path     : 'login',
    component: LoginPageComponent,
    data     : {title: 'login'}
  },
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {
    path        : '',
    loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate : [AuthGuard]
  },
  {
    path        : '',
    loadChildren: () => import('./features/products/products.module').then(m => m.ProductsModule),
    canActivate : [AuthGuard]
  },
  {path: '**', redirectTo: '/login'},
];

@NgModule({
  imports  : [RouterModule.forRoot(routes)],
  exports  : [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {
}
